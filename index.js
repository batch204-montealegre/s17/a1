// alert("hello");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

function printUserInfo(){

	let fullName = prompt("Enter Your Full Name:");
	let Age = prompt("Enter Your Age:");
	let location = prompt("Where is your location?");

	console.log("Hello," + " " +fullName);
	console.log("You are" + " "+Age + " " + "years old.");
	console.log("You live in" + " " +location);
}

printUserInfo();




	
// 	//first function here:

// /*
// 	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
// 		-invoke the function to display your information in the console.
// 		-follow the naming conventions for functions.

function printFaveBand(){

	console.log("1. Arctic Monkeys");
	console.log("2. Interpol");
	console.log("3. Metric");
	console.log("4. Yeah, yeah, Yeahs");
	console.log("5. The Killers");

}

printFaveBand();
	
// */

// 	//second function here:

// /*
// 	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
// 		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
// 		-invoke the function to display your information in the console.
// 		-follow the naming conventions for functions.

function printFaveMovies(){
	let RottenTomatoes = ("Rotten Tomatoes Rating:");


	console.log ("1. The Pianist");
	console.log (RottenTomatoes + "95%")

	console.log ("2. Little Prince");
	console.log (RottenTomatoes + "93%")

	console.log ("3. Hidden Figures");
	console.log (RottenTomatoes + "93%")

	console.log ("4. Beast of No Nation");
	console.log (RottenTomatoes + "91%")

	console.log ("5. Trainspotting");
	console.log (RottenTomatoes + "90%")
}

printFaveMovies();
	
// */
	
// 	//third function here:

// /*
// 	4. Debugging Practice - Debug the following codes and functions to avoid errors.
// 		-check the variable names
// 		-check the variable scope
// 		-check function invocation/declaration
// 		-comment out unusable codes.
// */

printUsers();
 function printUsers(){
 	
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


